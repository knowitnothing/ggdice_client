"""
This is a reference implementation for using ggdice.com without
a browser. Currently, only three message types are implemented here.
As an example, bets are continuously placed.

Note that ggdice.com is in constant modification, therefore this
code might eventually be out of sync in respect to the actual
protocol used in the live site.
"""
import time
import json
import urllib2
import cookielib
from decimal import Decimal

import websocket

BASE = 'ggdice.com'
HTTPS = 'https://%s' % BASE
COOKIE_NAME = 'yummy'
WSS = 'wss://%s/sock/websocket' % BASE

def get_cookie(name=COOKIE_NAME, debug=False):
    cj = cookielib.CookieJar()
    handler = []
    if debug:
        websocket.enableTrace(True)
        handler.append(urllib2.HTTPSHandler(debuglevel=1))
    cookie_handler = urllib2.HTTPCookieProcessor(cj)
    handler.append(cookie_handler)
    opener = urllib2.build_opener(*handler)
    # Make the request in order to capture the 'yummy' cookie.
    opener.open(HTTPS)
    # Get 'yummy's value.
    secret = None
    for cookie in cj:
        if cookie.name == name:
            # Actually this is the only cookie set by ggdice.com
            secret = cookie.value
            break
    return secret

class GGDiceSocket(object):
    """(Incomplete) Protocol used in ggdice.com"""

    msg_type_handler = {
        'base': 'gg_base',
        'bet': 'gg_betresult',
        'error': 'gg_error'}

    def __init__(self):
        self.csrf = None
        self.uid = None
        self.name = None
        self.balance = None

    def send(self, sock, data):
        """Helper function."""
        data['csrf'] = self.csrf
        sock.send(json.dumps(data))

    def gg_base(self, sock, base):
        """Base data sent right after a successful connection."""
        self.csrf = base['csrf']
        self.uid = base['uid']
        self.name = base['name']
        self.balance = Decimal(
                base['balance_%s' % base['curr_coin']]) / Decimal('1e8')
        if int(base['new']):
            # I'm a new user. We need to tell the site we are no
            # longer a new user in order to properly use the site.
            data = {'type': 'new'}
            self.send(sock, data)
            # Currently there is no confirmation for this message,
            # we just hope the connection is good and the server handles
            # it. Otherwise, the next time we connect we will need to
            # send this same message again.
            time.sleep(0.3)

    def gg_msgerror(self, sock, error):
        """Error due to not properly following the custom protocol."""
        print "gg error", error

    # Data from websocket.
    def on_message(self, sock, msg):
        data = json.loads(msg)
        if data['type'] not in self.msg_type_handler:
            #print 'Message of type %r ignored.' % data['type']
            return
        method = self.msg_type_handler[data['type']]
        getattr(self, method)(sock, data)
    def on_error(self, sock, err): print "websocket error: %s" % err
    def on_open(self, sock): print "websocket connected"
    def on_close(self, sock): print "websocket leaving"

def main(save_cookie, load_saved_cookie=True, sock=GGDiceSocket, args=None,
        debug=False):
    secret = None
    if load_saved_cookie:
        try:
            secret = open(save_cookie).read().strip()
        except IOError:
            # Assumption: a cookie was never obtained.
            pass
        else:
            print "Loaded cookie from '%s'" % save_cookie

    if secret is None:
        secret = get_cookie()
        if secret is None:
            print "Failed to get cookie."
            return
        print "New cookie obtained"
        if save_cookie:
            with open(save_cookie, 'w') as fobj:
                fobj.write('%s' % secret)
            print "Cookie saved to %s" % save_cookie

    if debug:
        websocket.enableTrace(True)

    ggdice = sock(*(args or ()))
    app = websocket.WebSocketApp(WSS,
            # Send received cookie.
            header=['Cookie: %s=%s' % (COOKIE_NAME, secret)],
            on_open=ggdice.on_open,
            on_message=ggdice.on_message,
            on_error=ggdice.on_error,
            on_close=ggdice.on_close)

    try:
        app.run_forever()
    except KeyboardInterrupt:
        pass

if __name__ == "__main__":
    main('account.cookie', load_saved_cookie=False)

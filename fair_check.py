import keccak # SHA-3

sha3 = keccak.sha3_256

def check_seed(secret, hsecret):
    """Check that the hash of the secret seed is the expected one."""
    expected = sha3(secret).hexdigest()
    return expected, expected == hsecret

def roll(secret, nonce, user_seed):
    """Perform a roll exactly as it is done at ggdice.com"""
    digest = sha3("%s:%d:%s" % (secret, nonce, user_seed)).hexdigest()
    result = int(digest, 16) % 1000000
    return result

def main(secret, hsecret, user_seed, nroll):
    if not check_seed(secret, hsecret)[1]:
        print "Unfair"
        return

    print "Rolls:"
    for i in xrange(1, nroll):
        nonce = i
        ri = roll(secret, nonce, user_seed)
        ri_str = str(ri).zfill(6)
        print "%d %s.%s" % (nonce, ri_str[:2], ri_str[2:])

if __name__ == "__main__":
    # The following data is a paste of the previous seed data
    # shown when a reseed is performed. You should verify that
    # the the nonce, user seed and secret hash match what you
    # expect.
    data = """
    8, 7326106401382237426,
    07a488c6cabfea4d26764cef0939bb89c1ada08fb3f69f34436c99df5b66fa52,
    dc6607a4276119704c6cf55bcbd5067e25977d51ce7c71a973e8916b1992a2ba
    """
    nonce, user_seed, hsecret, secret = [x.strip() for x in data.split(',')]
    main(secret, hsecret, user_seed, int(nonce))

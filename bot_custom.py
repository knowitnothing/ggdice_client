import sys
import hashlib

import bot_base

class MySocket(bot_base.GGDiceSocket):

    def __init__(self, cs_code):
        # Extend handlers for managing events regarding strategies.
        #
        # 'strategy-custom' is received after a custom strategy is verified.
        self.msg_type_handler['strategy-custom'] = 'gg_strategy_custom'
        # 'strategy' is received after a strategy is schedule for execution
        # or when it fails to get scheduled.
        self.msg_type_handler['strategy'] = 'gg_strategy'
        # 'strategy-summary' is received after a strategy finishes its
        # execution.
        self.msg_type_handler['strategy-summary'] = 'gg_strategy_summary'
        # 'strategy-partial-custom' is received each time a custom strategy
        # executes a certain number of bets. Currently, this is received
        # after at least 100 successful bets.
        self.msg_type_handler['strategy-partial-custom'] = 'gg_strategy_pres'
        # If the strategy being executed is a custom one, then a 'abort'
        # event will be received in case the parameters were ill-specified
        # or the strategy's code behaved badly.
        self.msg_type_handler['strategy-abort'] = 'gg_strategy_abort'

        self.sha = hashlib.sha256(cs_code).hexdigest()
        self.cs_code = cs_code  # The code for the custom strategy.

    def gg_base(self, sock, base):
        super(MySocket, self).gg_base(sock, base)

        # Connected, now we can send a custom strategy for verification.
        #
        # Note that this only need to be done once. After the custom
        # strategy has been accepted, you can run it directly without
        # going through this verification step.
        #
        data = {'type': 'strategy-custom',
                'public': True, # The code will be displayed on results
                'code': self.cs_code}
        self.send(sock, data)

    def gg_strategy_custom(self, sock, data):
        result = data['result']
        if 'hash' in result:
            # Custom strategy accepted.
            # Confirm that the code we sent is the code that will be used.
            assert result['hash'] == self.sha
            print "Custom strategy ok."
            # Run it.
            self._run_custom(sock, self.sha)
        else:
            print "Strategy not accepted. %s" % result['error']

    def gg_strategy(self, sock, data):
        if 'error' in data:
            # Strategy was not scheduled for execution. It could be
            # that there is some other strategy already executing,
            # or being verified, ..
            print "Strategy will not run. Reason: %s" % data['error']
            return
        print "Strategy scheduled: %s" % data['result']

    def gg_strategy_summary(self, sock, data):
        # Strategy finished
        print 'strategy summary', data

    def gg_strategy_pres(self, sock, data):
        print 'strategy partial results', data['result']

    def gg_strategy_abort(self, sock, data):
        print 'Strategy aborted. Reason: %s' % data['result']['error']

    def _run_custom(self, sock, sha_hash):
        strat = {'type': 'strategy',
                 'custom': True,
                 # Strategy identifier.
                 'hash': sha_hash,
                 # How much to dedicate to it, in floating point.
                 'bankroll': str(self.balance / 2),
                 # Run with fakecoins. This assumes the user is already
                 # in that room since the balance above is assumed to
                 # be from that room.
                 'room': 1
                 }
        self.send(sock, strat)


if __name__ == "__main__":
    custom_strategy = open(sys.argv[1]).read()
    bot_base.main('cookie', sock=MySocket, args=(custom_strategy, ))
